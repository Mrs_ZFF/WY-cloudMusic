import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home/index.vue'
import Layout from '../views/Layout.vue'


Vue.use(VueRouter)

export default new VueRouter({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'layout',
            component: Layout,
            redirect: 'home',   // 默认加载中子路由home的path
            children: [
                {
                    path: 'home',
                    name: 'home',
                    component: Home
                },
                {
                    path: 'topic',
                    name: 'topic',
                    component: () => import('../views/Topic.vue') //不要一上来就加载，需要时再加载
                },
                {
                    path: 'mine',
                    name: 'mine',
                    component: () => import('../views/Mine/Mine.vue') //不要一上来就加载，需要时再加载
                },
                {
                    path: 'search',
                    name: 'search',
                    component: () => import('../views/Search.vue') //不要一上来就加载，需要时再加载
                }
            ]
        },
        {
            path: '/newSongMore',
            name: 'newSongMore',
            component: () => import('../views/Home/NewSongMore.vue')
        },
        {
            path: '/recommendDetail',
            name: 'recommendDetail',
            component: () => import('../views/Home/RecommendDetail.vue')
        },
        {
            path: '/player',
            name: 'player',
            component: () => import('../views/Player.vue')
        }
    ]
})