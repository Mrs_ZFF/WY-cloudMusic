import Vue from 'vue'

import App from './App.vue'

//引入路由规则
import router from './router/index.js'

//引入elementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import '../src/assets/css/common.css'

//引入vant2UI
import { Icon, NavBar, Pagination, Search, Form, Field, Button, List, Cell, Tab, Tabs } from 'vant';



Vue.config.productionTip = false

Vue.use(ElementUI)

Vue.use(Icon);
Vue.use(NavBar);
Vue.use(Pagination);
Vue.use(Search);
Vue.use(Form);
Vue.use(Field);
Vue.use(Button);
Vue.use(List);
Vue.use(Cell);
Vue.use(Tab);
Vue.use(Tabs);


new Vue({
  render: h => h(App),
  router,
  beforeCreate() {
    Vue.prototype.$bus = this   //定义全局事件总线
  }
}).$mount('#app')
