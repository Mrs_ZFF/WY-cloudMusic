# 仿网易云音乐

#### 介绍
 **技术栈** 
Vue、Vue-Router、Vue-Axios、Element UI、Vant UI

 **项目描述** 
    该音乐播放器调用的网易云NodeJS版API数据接口，可以播放网易云音乐平台的歌曲，部分歌曲播放需要登录或网易云会员，用户可以在登录界面登录自己的网易云账号。首页同步了网易云音乐的新歌速递和推荐歌单，用户可以收听自己喜欢的音乐，同时可以在搜索页面搜索想听的歌曲。

 **项目功能** 

1. 新歌速递：同步了网易云音乐的华语、欧美、日本、韩国四个分类的新歌。
2. 推荐歌单：推荐了不同类型的歌单，歌单里有这一类型的不同歌曲。
3. 登录功能：输入手机号和密码登录网易云账号，可以获取个人信息。
4. 播放功能：点击歌曲后进入播放页面，可获得歌曲信息、音乐和歌词。
5. 歌词滚动：播放音乐时，歌词会同步音乐播放的位置而滚动。
6. 搜索功能：在搜索栏里输入关键字可以搜索出相关的歌曲。


#### 软件架构
软件架构说明


#### 安装教程

1.  git clone
2.  安装网易云NodeJs版API，文档地址：https://binaryify.github.io/NeteaseCloudMusicApi
3.  xxxx

#### 使用说明

1.  运行数据接口：node app.js
2.  安装依赖：npm install
3.  启动：npm run server


#### 项目预览

![输入图片说明](https://foruda.gitee.com/images/1701942841015029655/691a217e_12435261.png "首页.png")
![输入图片说明](https://foruda.gitee.com/images/1701942902688944100/cd969bae_12435261.png "更多.png")
![输入图片说明](https://foruda.gitee.com/images/1701942922698413468/8cc45275_12435261.png "歌单.png")
![输入图片说明](https://foruda.gitee.com/images/1701942937597044294/a760a4ec_12435261.png "搜索.png")
![输入图片说明](https://foruda.gitee.com/images/1701942954433393360/23d39aff_12435261.png "播放.png")
![输入图片说明](https://foruda.gitee.com/images/1701942964803645510/0a00e454_12435261.png "登录.png")



#### 实现步骤
1. 解决跨域：在vue.config.js中配置代理服务器。

2. 新歌分类切换实现：
① 先写好一个请求方法；
② data中存入初始参数，在mounted钩子中将初始参数存入请求方法，调用请求方法，从而页面为初始页面；
③ 先给每个li中的span设置好自定义id，这个自定义id为请求中url的参数值，利用事件委托给导航栏ul绑定点击事件，回调函数中通过事件对象获取目标的自定义id值，并赋值给data中的初始参数，接着将新参数传入请求方法并调用请求方法，从而页面重新渲染，实现新歌分类的切换。

3. 音乐播放实现：通过路由跳转url传参歌曲id，从而通过歌曲id调用歌曲信息和歌词。

4. 歌词滚动实现：
① 歌词解析：通过设定正则规则和数组遍历,分割时间和歌词，转化时间格式获得总秒数，将时间作为属性名，时间对应的歌词作为属性值存入一个对象。即可通过v-for遍历对象渲染页面。
② 歌词滚动：给audio绑定timeupdate事件，获取当前播放位置currentTime，当currentTime大于当行歌词对应的时间，且小于下一行歌词对应的时间，则给当行歌词添加高亮样式，给歌词的父类盒子设置好相对位置positon：relative，通过具有高亮样式当行的歌词到父类盒子顶部的offsetTop，赋值给父类盒子的内容垂直滚动的像素数scrollTop，从而实现歌歌词随播放时间滚动的功能。

5. 登录状态保持实现：
    在mounted钩子中调用登录状态接口，若接口返回的数据存在账号信息，则保持已登录界面，若无则是登录界面。在调用登录状态接口中遇到了缓存问题，同一个url短时间内只能向同一个接口请求一次，导致登录状态接口的数据存在更新延迟，因此我通过Date.now()获取当前时间戳来作为url参数，使每次请求的url都不一样，从而实现登录状态的保持。



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
